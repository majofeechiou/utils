export default function filterItemByKey( sary, str_key, str_filter ) {
    if( (typeof str_key === 'string') && (str_key.trim()!=='') && (typeof str_filter === 'string') && (str_filter.trim()!=='') ){
        var _sary_output = [],
            _json_item = {},
            _data_vaule = '';
        let _obj_reg = new RegExp(str_filter, 'gim');
        for( let i=0; i<sary.length; i++ ){
            _json_item = sary[i];
            _data_vaule = _json_item[str_key];
            if( _data_vaule && (typeof _data_vaule === 'string') && ((_data_vaule.match(_obj_reg) instanceof Array === true) || ((_data_vaule.toLowerCase()).indexOf(str_filter.toLowerCase())>=0)) ){
                _sary_output.push(_json_item);
            }
        }
        return _sary_output;
    }else{
        return sary;
    }
}