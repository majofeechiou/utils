export default function findItemByKey( sary, str_key, data_value, str_key_show ) {
    var _json_output = {};
    if( sary instanceof Array === true ){
        for( let i=0; i<sary.length; i++ ){
            if( JSON.stringify(sary[i][str_key])===JSON.stringify(data_value) ){
                _json_output = sary[i];
                break;
            }
        }
    }
    if( typeof str_key_show === 'string' && str_key_show.trim()!=='' ){
        if( Object.keys(str_key_show).indexOf(str_key_show) ){
            return _json_output[str_key_show];
        }else{
            return '';
        }
    }else{
        return _json_output;
    }
}