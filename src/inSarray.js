export default function inSarray( sary, str_key, data_value ) {
    var _num_match = -1;
    if( sary instanceof Array === true ){
        for( let i=0; i<sary.length; i++ ){
            if( JSON.stringify(sary[i][str_key])===JSON.stringify(data_value) ){
                _num_match = i;
                break;
            }
        }
    }
    return _num_match;
}