// https://derickbailey.com/2015/09/23/how-to-get-the-difference-between-two-arrays-as-a-distinct-list-with-javascript-es6/
// http://www.2ality.com/2014/05/es6-array-methods.html
export default function getDiffArray( ary_completeList, ary_invalidList ) {
    var _ary_validList = ary_completeList.filter((item) => {
        return !new Set(ary_invalidList).has(item);
    });
    return _ary_validList;
}
